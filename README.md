# Panda Pay API Wrapper
This project is stable; however, has limited features at this time. 
If you would like to request a feature, please email [support@ellytronic.media](mailto:support@ellytronic.media).
You can also submit a pull request using the git URL above.

## Install Instructions
We recommend using [composer](http://getcomposer.org) for this application. Using composer, run the following command:

`composer require ellytronic/panda-pay-api-wrapper`

## Usage Instructions
Coming soon. For immediate assistance, review the sample code and unit tests as described below.

### Sample Code
There is a selection of sample code in the "sample-code" folder. 
To use the the sample code, first copy *cfg\.env.example* to *cfg\.env*. 
Search for "@todo" in the folder for items you should change. 
These excerpts are intended as a guide only. 
The sample code requires PHP 7+ to run.


### Unit Tests
To run the unit tests, first copy *cfg\.env.example* to *cfg\.env*. 
Next, update the values in the environment file with your secret data. 
This file is ignored by git and will not be automatically tracked.

<small>Last updated: November 27, 2017</small>