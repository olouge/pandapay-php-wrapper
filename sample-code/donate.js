Panda.on('success', function(cardToken) {
    jQuery(".source", "#payment-details").val(cardToken);
    submitDonation();
});

Panda.on('error', function(errors) {
    // errors is a human-readable list of things that went wrong
    //  (invalid card number, missing last name, etc.)

    removeClonedAlerts();

    var err_ul = "<ul>";
    $.each(errors, function(){
        err_ul += "<li>" + this.message + "</li>";
    });
    err_ul += "</ul>";
    var err_alert = getBasicError("We found the following errors in your submission", err_ul);
    $("#payment-details").after(err_alert);
});


jQuery(document).ready(function($){
    /**
     * Auto space the credit card field
     * @see https://stackoverflow.com/a/36568611/1952517
     */
    $('#card-number').on('keypress change', function () {
        $(this).val(function (index, value) {
            var val = value.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
            if ($.trim(val).length > (4*4 + 3))
                $("#card-number").css("color", "red");
            else
                $("#card-number").css("color", "inherit");
            return val;
        });
    });
    $('#card-number').on('paste', function () {
        $(this).val(function (index, value) {
            return value.replace(/\W/gi, '').replace(/(.{4})/g, '$1 ');
        });
        setTimeout(function(){
            $('#credit-card').trigger("change");
        });
    });

    /**
     * Update the expiry field for panda pay
     */
    $(".expire-update").on("change", function(){
        $("#expire-full").val(
            $("#expire-month").val() + "/" + $("#expire-year").val()
        );
    });

    /**
     * Format the amount
     */
    $("#amount").autoNumeric('init', {
        vMin: '0',
        aSign: '$'
    });
});

function submitDonation()
{
    var $ = jQuery;
    var $form = $("#payment-details");
    var $submit = $("#tokenize");

    // check if a request is already pending
    if ($submit.is(":disabled"))
        return false;

    // hide errors, update and disable submit
    removeClonedAlerts();
    $submit.html("Please wait...").prop("disabled", true);

    $.post('index.php', $form.serialize())
        .done(function(response){
            // console.debug(response);
            try {
                var feedback = JSON.parse(response);
                // console.debug(feedback);

                if (feedback.success === true) {
                    $submit.remove();
                    var conf_alert = getBasicSuccess(feedback.message, feedback.extra);
                    $("#payment-details").after(conf_alert);

                } else {
                    console.error("Create Donation Failure:");
                    console.error(feedback);
                    console.error(response);
                    var conf_alert = getBasicError(feedback.message, feedback.extra);
                    $("#payment-details").after(conf_alert);
                }
            } catch (e) {
                console.error("Create Donation Failure - error:");
                console.error(e);
                console.error("create donation server response: " + response);
                $errors.before(getBasicError("Server error", http500_en)); //effect("shake");
                var err_alert = getBasicError("Server error", "A server error occurred. Please try again later.");
                $("#payment-details").after(err_alert);
            }
        })

        .fail(function(response){
            console.error("500 Error! Exception details:");
            console.error(response);
            var err_alert = getBasicError("Server error", "A server error occurred. Please try again later.");
            $("#payment-details").after(err_alert);
        })

        .always(function(r){
            $submit.html("Donate").prop("disabled", false);
        })

}


/* -------------------
-- Responses
------------------- */

/**
 * return a basic response that follows theme formatting
 * @param  classes       the response box CSS classes
 * @param  title         the title to display in the response
 * @param  message       the details to display next to the title
 * @return void
 */
function getBasicResponse(classes, title, message) {
    var $r =  $(".sample-alert").clone();
    $r.html($r.html().replace("{{ class }}", classes)
            .replace("{{ title }}", title)
            .replace("{{ msg }}", message));

    $r.css("display", "block");
    $r.addClass("cloned-alert");
    return $r;
}
function getBasicError(title, message, extraClasses) {
    if (typeof extraClasses === 'undefined')
        extraClasses = '';
    return getBasicResponse(
        "alert-danger " + extraClasses,
        title,
        message
    );
}
function getBasicSuccess(title, message, extraClasses) {
    if (typeof extraClasses === 'undefined')
        extraClasses = '';
    return getBasicResponse(
        "alert-success " + extraClasses,
        title,
        message
    );
}
function removeClonedAlerts() {
    $(".cloned-alert").remove();
}