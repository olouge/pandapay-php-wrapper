<?php
/**
 * Creates a log stream connection to Loggly
 */

namespace PP\Loggers;

use Monolog\Formatter\LogglyFormatter;
use Monolog\Handler\LogglyHandler;
use Monolog\Logger;

class LogglyFactory {

	/**
	 * Creates a log stream connection to Loggly
	 * @param String $appName the name of the app to record in loggly
	 * @param String $key the secret API key/token
	 * @return Logger the log stream
	 */
	public static function createStream($appName, $key) {
		$stream = new LogglyHandler($key . '/tag/monolog', Logger::INFO);
		$stream->setFormatter(new LogglyFormatter());

		$log = new Logger($appName);
		$log->pushHandler($stream);

		return $log;
	}
}