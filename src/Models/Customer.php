<?php

namespace PP\Models;

/**
 * Represents a customer
 * 
 * @author  Olouge Eya <olongel92@gmail.com>
 * @link    https://pandapay.io
 */

/*
id - Unique id, prefixed with ch_
object - Set to "donation"
created - Timestamp when the object was created
livemode - "True" if a live key was used, "False" is a test key was used
currency (required) - Only “usd” is available at this time
charge_amount - An integer representing the total amount charged to the credit card, in cents
destination_ein - The tax ID of the charity receiving this donation (optional)
donation_after_fees - An integer representing the amount of the donation after fees, in cents
source (required) - The funding source for this donation; either a Panda.js card token or a Customer id with a valid payment source
grants - A list of any Grant objects that were created with the funds from this donation
receipt_email (required) - The email which should receive a donation receipt
restricted (default: null) - Use this parameter to set the plain text name of a program or project at your destination_ein, to which you’d like to designate funds. 
*/

class Customer {
    private $id = null;
    private $object = "customer";
    private $created = null;
    private $livemode = false;
    private $currency = "usd";
    private $charge_amount = null;
    private $destinationEIN = null;
    private $donation_after_fees = null;
    private $source = null;
    private $grants = array();
    private $receipt_email = null;
    private $restricted = null;

    public function __construct() {
    }

    /**
     * Sets the customer id
     * @param int $id Unique id, prefixed with ch_
     */
    public function setId($id)
    {
        $this->id = (int) $id;
    }

    /**
     * Sets the object field
     * @param String $obj the object
     */
    public function setObject($obj)
    {
        $this->object = (string) $obj;
    }

    /**
     * Sets the created field
     * @param String $created the created timestamp
     */
    public function setCreated($created)
    {
        $this->created = (string) $created;
    }

    /**
     * Sets the livemode field
     * @param Boolean $livemode to indicate if it is a live key used or test key. "True" if a live key was used, "False" is a test key was used
     */
    public function setLivemode($livemode)
    {
        $this->livemode = (boolean) $livemode;
    }

    /**
     * Sets the currency field
     * @param String $currency to keep track of the currency used. Only “usd” is available at this time
     */
    public function setCurrency($currency)
    {
        $this->currency = (string) $currency;
    }

    /**
     * Sets the charge_amount field
     * @param int $charge_amount the amount in cents. An integer representing the total amount charged to the credit card, in cents.
     */
    public function setChargeamount($chargeamount)
    {
        $this->charge_amount = (int) $chargeamount; // in cents
    }

    /**
     * Sets the destination EIN
     * @param String $destinationEIN the EIN. The tax ID of the charity receiving this donation (optional)
     */
    public function setDestinationEIN($ein)
    {
        $this->destinationEIN = (string) $ein;
    }

    /**
     * Sets the donation_after_fees field
     * @param int $donation_after_fees the amount in cents. An integer representing the amount of the donation after fees, in cents
     */
    public function setDonationafterfees($donationafterfees)
    {
        $this->donation_after_fees = (int) $donationafterfees; // in cents
    }

    /**
     * Sets the payment source
     * @param String $source the tokenized cc. The funding source for this donation; either a Panda.js card token or a Customer id with a valid payment source
     */
    public function setSource($source)
    {
        $this->source = (string) $source;
    }

    /**
     * Sets the grants fee amount
     * @param array $grants A list of any Grant objects that were created with the funds from this donation
     */
    public function setGrants($grants)
    {
        $this->grants = (array) $grants;
    }

    /**
     * Sets the email
     * @throws \InvalidArgumentException on bad email
     * @param String $email the email
     */
    public function setReceiptEmail($em) 
    {
        if (!filter_var($em, FILTER_SANITIZE_EMAIL))
            throw new \InvalidArgumentException("Invalid email address: $em");
        
        $this->receiptEmail = $em;
    }

    /**
     * Sets the restricted field
     * @param String $restricted. Use this parameter to set the plain text name of a program or project at your destination_ein, to which you’d like to designate funds. 
     */
    public function setRestricted($restricted)
    {
        $this->restricted = (string) $restricted;
    }

    /**
     * gets the customer id
     * @return int $id Unique id, prefixed with ch_
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * gets the object field
     * @return String $obj the object
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * gets the created field
     * @return String $created the created timestamp
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * gets the livemode field
     * @return Boolean $livemode to indicate if it is a live key used or test key. "True" if a live key was used, "False" is a test key was used
     */
    public function getLivemode()
    {
        return $this->livemode;
    }

    /**
     * gets the currency field
     * @return String $currency to keep track of the currency used. Only “usd” is available at this time
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    /**
     * gets the charge_amount field
     * @return int $charge_amount the amount in cents. An integer representing the total amount charged to the credit card, in cents.
     */
    public function getChargeamount()
    {
        return $this->charge_amount; // in cents
    }

    /**
     * gets the destination EIN
     * @return String $destinationEIN the EIN. The tax ID of the charity receiving this donation (optional)
     */
    public function getDestinationEIN()
    {
        return $this->destinationEIN;
    }

    /**
     * gets the donation_after_fees field
     * @return int $donation_after_fees the amount in cents. An integer representing the amount of the donation after fees, in cents
     */
    public function getDonationafterfees()
    {
        return $this->donation_after_fees; // in cents
    }

    /**
     * gets the payment source
     * @return String $source the tokenized cc. The funding source for this donation; either a Panda.js card token or a Customer id with a valid payment source
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * gets the grants fee amount
     * @return array $grants A list of any Grant objects that were created with the funds from this donation
     */
    public function getGrants()
    {
        return $this->grants;
    }

    /**
     * gets the email
     * @return String $email the email
     */
    public function getReceiptEmail() 
    {
        return $this->receiptEmail;
    }

    /**
     * gets the restricted field
     * @return String $restricted. Use this parameter to set the plain text name of a program or project at your destination_ein, to which you’d like to designate funds. 
     */
    public function getRestricted()
    {
        return $this->restricted;
    }

}
