<?php

namespace PP\Models;

/**
 * Represents a grant
 * 
 * @author  Olouge Eya <olongel92@gmail.com>
 * @link    https://pandapay.io
 */

/*
id - Unique id, prefixed with gr_
object - Set to "grant"
created - Timestamp when the object was created
livemode - "True" if a live key was used, "False" is a test key was used
amount (required) - An integer representing the amount of the grant in cents
destination_ein (required) - The tax ID of the charity receiving this grant
source_transaction - The donation id from which these funds were drawn, if any
restricted (default: null) - Use this parameter to set the plain text name of a program or project at your destination_ein, to which you’d like to designate funds. 
*/

class Grant {
    private $id = null;
    private $object = "grant";
    private $created = null;
    private $livemode = false;
    private $amount = null;
    private $destinationEIN = null;
    private $source_transaction = null;
    private $restricted = null;

    public function __construct() {
    }

    /**
     * Sets the grant id
     * @param int $id Unique id, prefixed with gr_
     */
    public function setId($id)
    {
        $this->id = (int) $id;
    }

    /**
     * Sets the object field
     * @param String $obj the object
     */
    public function setObject($obj)
    {
        $this->object = (string) $obj;
    }

    /**
     * Sets the created field
     * @param String $created the created timestamp
     */
    public function setCreated($created)
    {
        $this->created = (string) $created;
    }

    /**
     * Sets the livemode field
     * @param Boolean $livemode to indicate if it is a live key used or test key. "True" if a live key was used, "False" is a test key was used
     */
    public function setLivemode($livemode)
    {
        $this->livemode = (boolean) $livemode;
    }

    /**
     * Sets the amount field
     * @param int $amount the amount in cents. An integer representing the amount of the grant in cents
     */
    public function setAmount($amount)
    {
        $this->amount = (int) $amount; // in cents
    }

    /**
     * Sets the destination EIN
     * @param String $destinationEIN the EIN. The tax ID of the charity receiving this grant (optional)
     */
    public function setDestinationEIN($ein)
    {
        $this->destinationEIN = (string) $ein;
    }

    /**
     * Sets the payment source_transaction
     * @param String $source_transaction. The donation id from which these funds were drawn, if any
     */
    public function setSourcetransaction($sourcetrans)
    {
        $this->source_transaction = (string) $sourcetrans;
    }

    /**
     * Sets the restricted field
     * @param String $restricted. Use this parameter to set the plain text name of a program or project at your destination_ein, to which you’d like to designate funds. 
     */
    public function setRestricted($restricted)
    {
        $this->restricted = (string) $restricted;
    }

    /**
     * gets the grant id
     * @return int $id Unique id, prefixed with gr_
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * gets the object field
     * @return String $obj the object
     */
    public function getObject()
    {
        return $this->object;
    }

    /**
     * gets the created field
     * @return String $created the created timestamp
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * gets the livemode field
     * @return Boolean $livemode to indicate if it is a live key used or test key. "True" if a live key was used, "False" is a test key was used
     */
    public function getLivemode()
    {
        return $this->livemode;
    }

    /**
     * gets the amount field
     * @return int $amount the amount in cents. An integer representing the amount of the grant in cents
     */
    public function getAmount()
    {
        return $this->amount; // in cents
    }

    /**
     * gets the destination EIN
     * @return String $destinationEIN the EIN. The tax ID of the charity receiving this donation (optional)
     */
    public function getDestinationEIN()
    {
        return $this->destinationEIN;
    }

    /**
     * gets the payment source_transaction
     * @return String $source_transaction. The donation id from which these funds were drawn, if any
     */
    public function getSourcetransaction()
    {
        return $this->source_transaction;
    }

    /**
     * gets the restricted field
     * @return String $restricted. Use this parameter to set the plain text name of a program or project at your destination_ein, to which you’d like to designate funds. 
     */
    public function getRestricted()
    {
        return $this->restricted;
    }
}
