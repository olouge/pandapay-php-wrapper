<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <title>Panda Pay API Wrapper</title>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.4/jquery.min.js"></script>

    <!-- bootstrap for styling -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous">
</head>
<body>
<div class="container" style="margin-top: 50px;">
    <div class="row">
        <div class="col-4 offset-2">
            <h1>Donate Form</h1>
        </div>
    </div>

    <form id="panda-cc-tokenizer">
        <div class="row">
            <section class="col-4 offset-2">
                <div class="form-group">
                    <input data-panda="first_name" required placeholder="First Name" class="form-control">
                </div>

                <div class="form-group">
                    <input data-panda="last_name" required placeholder="Last Name" class="form-control">
                </div>

                <div class="form-group">
                    <input data-panda="email" name="email" type="email" form="payment-details" required placeholder="Email" class="form-control">
                </div>
            </section>

            <section class="col-4">
                <div class="form-group">
                    <input id="amount" name="amount" form="payment-details" required placeholder="Amount" class="form-control">
                </div>

                <div class="form-group">
                    <section class="card-number-cvv-container">
                        <div class="row">
                            <div class="col-9">
                                <input id="card-number" data-panda="credit_card" maxlength="19" required placeholder="Card Number" class="form-control">
                            </div>
                            <div class="col-3">
                                <input id="card-cvv" minlength="3" maxlength="3" data-panda="cvv" required placeholder="CVV" class="form-control">
                            </div>
                        </div>
                    </section>
                </div>

                <div class="form-group">
                    <section class="expiration-container clearfix">
                        <div class="row">
                            <div class="col-4">
                                <select id="expire-month" class="expire-update form-control">
                                    <option value="" disabled selected>MM</option>
                                    <?php
                                    for ($i = 1; $i < 13; $i++) {
                                        ?>
                                        <option value="<?=$i?>"><?=$i?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-1">
                                <span id="expire-divisor">/</span>
                            </div>
                            <div class="col-4">
                                <select id="expire-year" class="expire-update form-control">
                                    <option value="" disabled selected>YYYY</option>
                                    <?php
                                    $y = (int) date("Y");
                                    for ($i = $y; $i < $y+15; $i++) {
                                        ?>
                                        <option value="<?=$i?>"><?=$i?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-3">
                                <button id="tokenize" class="btn btn-primary submit">Donate</button>
                                <input
                                    value="1/<?=$y?>"
                                    type="hidden"
                                    id="expire-full"
                                    data-panda="expiration"
                                    required
                                >
                            </div>
                        </div>
                    </section>
                </div>
            </section>
        </div>
    </form>


    <form id="payment-details">
        <input type="hidden" name="source" class="source" value="">
        <input type="hidden" name="charity-ein" value=""> <!-- @todo add this value if you want -->
        <input type="hidden" name="action" value="createDonation">
        <!-- @todo you should add a nonce here in production -->
    </form>

    <div class="sample-alert" style="display: none;">
        <div class="row">
            <div class="col-8 offset-2">
                <div class="alert {{ class }} alert-dismissible fade show" role="alert">
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                    <strong>{{ title }}</strong> {{ msg }}
                </div>
            </div>
        </div>
    </div>

</div>
<script src="auto-numeric/autoNumeric.js"></script>
<script src="<?=PANDAPAY_JS_URL?>"></script>
<script>
    Panda.init('<?=PANDAPAY_PUBLIC_KEY?>', 'panda-cc-tokenizer');
</script>
<script src="donate.js"></script>
</body>
</html>
